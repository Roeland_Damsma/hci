#!/usr/bin/python3
#Roeland Damsma & Merlijn van der Baan
#s2972654 & s2757656
import tkinter as tk
from tkinter import ttk
import praw
import threading
from tkinter import simpledialog
from tkinter import messagebox
import queue
#Class to display comment trees


class CommentTreeDisplay(tk.Frame):
	#initial class for gui
	def __init__(self, master, queue1, queue2):
		tk.Frame.__init__(self,master)
		self.master = master
		self.topframe = tk.Frame(self)
		self.topcommentqueue = queue1
		self.lowcommentqueue = queue2
		self.tree = ttk.Treeview(self.master)
		#treeview
		self.tree["columns"]=("one","two")
		self.tree.column("one", width=100 )
		self.tree.column("two", width=800)
		self.tree.heading("one", text="Username")
		self.tree.heading("two", text="Comment")
		#menubar
		self.menubar = tk.Menu(self.topframe)
		self.filemenu = tk.Menu(self.menubar, tearoff=0)
		#filemenu
		self.filemenu.add_command(label="Quit!", command=self.master.quit)
		self.menubar.add_cascade(label="File", menu=self.filemenu)
		#processing menu
		self.processingmenu = tk.Menu(self.menubar, tearoff=0)
		self.processingmenu.add_command(label="Load Comments", command=self.get_url)
		self.menubar.add_cascade(label="Processing", menu=self.processingmenu)
		self.master.config( menu=self.menubar)
		self.topframe.pack()
		self.tree.pack()

		self.reddit = praw.Reddit(client_id='XnZ69BVaeiyCOA',
								  client_secret='P1s87TxT4gi-4z7tdLoUbMWh-HI',
								  password='papflap66',
								  user_agent='testscript by /u/HC_interaction_Henk',
								  username='HC_interaction_Henk')
		
		#funcition to do PRAW calculations

	def update_top_level_comments(self):
		#updates gui with toplevel from queue
		try:
			self.submission = self.topcommentqueue.get(block=False)
			if self.submission is not None:
				try:
					self.tree.insert("", 3, self.submission.id, text=self.submission.id, values=(self.submission.author, self.submission.body))
				except tk.TclError:
					pass

		except queue.Empty:
			pass
		self.after(100, self.update_top_level_comments)

	def update_low_level_comments(self):
		#updates gui with low level comments
		try:
			self.comment = self.lowcommentqueue.get(block=False)
			if self.comment is not None:
				try:
					self.tree.insert(self.comment.parent_id[3:], 3, self.comment.id, text=self.comment.id, values=(self.comment.author, self.comment.body))
				except tk.TclError:
					pass
		except queue.Empty:
			pass
		self.after(100, self.update_low_level_comments)

	def get_url(self):
		#takes url from user
		self.redditurl = simpledialog.askstring("Input", "Please insert Reddit url")
		self.get_comments(self.redditurl)

	def get_comments(self, redditurl):
		#gets comments from reddit and puts them in a queue
		try:
			self.redditurl = redditurl
			self.submission = self.reddit.submission(url=self.redditurl)
			self.top_level_list = []
			self.submission.comments.replace_more(limit=None)
			for self.top_level_comment in self.submission.comments:
				self.top_level_list.append(self.top_level_comment.id)
				self.topcommentqueue.put(self.top_level_comment)

			self.submission.comments.replace_more(limit=None)
			for self.comment in self.submission.comments.list():
				if self.comment.id not in self.top_level_list:
					self.lowcommentqueue.put(self.comment)

		except praw.exceptions.ClientException:
			messagebox.showerror("Error", "This URL is invalid")


class ProcessComments:

	def __init__(self, master):
		#star threads
		self.master = master
		self.topcommentqueue = queue.Queue()
		self.lowcommentqueue = queue.Queue()
		self.frame = CommentTreeDisplay(self.master, self.topcommentqueue,self.lowcommentqueue)
		self.frame.pack()

		self.thread1 = threading.Thread(target=self.frame.get_comments)
		self.thread1.start()

		self.thread2 = threading.Thread(target=self.frame.update_top_level_comments)
		self.thread2.start()

		self.thread3 = threading.Thread(target=self.frame.update_low_level_comments)
		self.thread3.start()

	def clear_queue(self):
		#clears queue for reloading comments in part4.py
		self.topcommentqueue.queue.clear()
		self.lowcommentqueue.queue.clear()

	def get_redditurl(self):
		return self.frame.redditurl



# set commentTreeDisplay as root and run as mainloop
#root = tk.Tk()
#comment_stream = ProcessComments(root)
#root.mainloop()
