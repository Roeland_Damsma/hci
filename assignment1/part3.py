#!/usr/bin/python3
#Roeland Damsma & Merlijn van der Baan
#s2972654 & s2757656
import tkinter as tk
from tkinter import ttk
import praw
import threading
import part2
from tkinter import messagebox


class ResponseCommentTreeDisplay(part2.ProcessComments):

    def __init__(self, master):
        part2.ProcessComments.__init__(self, master)
        self.master = master
        self.frame.tree.bind("<Double-1>", self.on_click)
        self.reddit = praw.Reddit(client_id='XnZ69BVaeiyCOA',
                                  client_secret='P1s87TxT4gi-4z7tdLoUbMWh-HI',
                                  password='papflap66',
                                  user_agent='testscript by /u/HC_interaction_Henk',
                                  username='HC_interaction_Henk')

    def on_click(self,event):
        #gives user option to click and then comment on reddit
        self.item = self.frame.tree.selection()[0]
        print("you clicked on", self.frame.tree.item(self.item, "text"))
        self.comment = self.reddit.comment(id=self.item)
        self.response = tk.simpledialog.askstring('Input', "Please enter a comment ", parent=self.master)
        if self.response is not "" or None:
            try:
                self.comment.reply(self.response)
                messagebox.showinfo("Comment posted", "You successfully placed a comment.")
            except praw.exceptions.APIException:
                messagebox.showerror("Error!", "Something went wrong ")


#root = tk.Tk()
#comment_tree = ResponseCommentTreeDisplay(root)
#root.mainloop()
