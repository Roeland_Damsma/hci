#!/usr/bin/python3
#Roeland Damsma en Merlijn van der Baan
import tkinter as tk
from tkinter import ttk
import praw 
import time
import threading


#main class incomingsubmissions
class IncomingSubmissions(tk.Tk):

    #initialization of gui window
    def __init__(self):
        tk.Tk.__init__(self)
        self.container = tk.Frame(self,width=900, height=300)
        self.tree=ttk.Treeview(self.container)
        self.tree['show']='headings'
        self.tree['columns']=('Subreddit','Submission')
        self.tree.column('Subreddit', width= 150)
        self.tree.column('Submission', width= 750)
        self.tree.heading('Subreddit', text='Subreddit')
        self.tree.heading('Submission', text='Submission')
        S = ttk.Scrollbar(self.container, orient="vertical", command=self.tree.yview)
        S.pack(side='right', fill='y')
        self.tree.configure(yscrollcommand=S.set)
        self.container.pack()
        self.tree.pack()
        self.x = tk.IntVar()
        self.e1 = tk.Entry(self.container)
        self.e1.pack()
        self.b1 = tk.Button(self.container, text='Add to whitelist', command=self.addwhitelist)
        self.b1.pack()
        self.b2 = tk.Button(self.container, text='Add to blacklist', command=self.addblacklist)
        self.b2.pack()
        self.b3 = tk.Button(self.container, text='Clear all lists', command=self.clearlists)
        self.b3.pack()
        self.b4 = tk.Button(self.container, text='pause', command=self.pause_button)
        self.b4.pack()
        self.whitelist = []
        self.blacklist = []
        t1 = threading.Thread(target=self.getSubmission)
        t1.start()
        t2 = threading.Thread(target=self.setSlider)
        t2.start()

    def pause_button(self):
        if pressed ==False:
            pressed = True
        elif pressed== True:
            pressed= False

    def play_button(self,pressed):
        self.pressed = True

    def is_on(self,pressed):
        return self.pressed

    def addwhitelist(self):
        self.whitelist.append(self.e1.get())
        self.e1.delete(0, tk.END)
        print(self.whitelist)

    def addblacklist(self):
        self.blacklist.append(self.e1.get())
        print(self.blacklist)

    def clearlists(self):
        self.blacklist = []
        self.whitelist = []

    def subredditExist(self, sub):
        exists = True
        try:
            reddit.subreddits.search_by_name(sub, exact=True)
        except NotFound:
            exists = False
        return exists


    def getSubmission(self):
        #reddit credentials
        reddit = praw.Reddit(client_id='XnZ69BVaeiyCOA',
                     client_secret='P1s87TxT4gi-4z7tdLoUbMWh-HI',
                     password='papflap66',
                     user_agent='testscript by /u/HC_interaction_Henk',
                     username='HC_interaction_Henk')
        #empty list to check if a submission is already in the treeview
        submissionlist = []
        #infinite loop to create constant stream of submissions
        pressed = True
        while True:
            try:
                #check if slider is on 0
                if self.x.get() != 0:
                    time.sleep(1)
                    #loop to get submissions at speed of the slider and to add them to treeview
                    for submission in reddit.subreddit('all').new(limit=self.x.get()):
                        if submission.title not in submissionlist and submission.subreddit not in self.blacklist and self.whitelist == []:
                            self.tree.insert('', 'end', text='', values=(submission.subreddit,submission.title))
                            self.tree.pack()
                            submissionlist.append(submission.title)
                    for submission in reddit.subreddit('all').new(limit=self.x.get()):
                        if submission.title not in submissionlist and submission.subreddit in self.whitelist and self.blacklist == []:
                            self.tree.insert('', 'end', text='', values=(submission.subreddit,submission.title))
                            self.tree.pack()
                            submissionlist.append(submission.title)
            #except to prevent app crashing when unknown characters are in submission
            except tk.TclError:
                pass
    #function for the slider
    def setSlider(self):
        slider = tk.Scale(self.container, from_=0, to_=100, variable= self.x, orient=tk.HORIZONTAL)
        slider.pack()       

 


#set incomingsubmissions as root and run root as mainloop
root = IncomingSubmissions()
root.title('part1.py')
root.mainloop()
