#!/usr/bin/python3
#Roeland Damsma en Merlijn van der Baan
import tkinter as tk
from tkinter import ttk
import praw
import threading
from tkinter import simpledialog
from tkinter import messagebox
#Class to display comment trees
class CommentTreeDisplay(tk.Tk):
	#initial class for gui
	def __init__(self):
		tk.Tk.__init__(self)
		self.container = tk.Frame(self, width = 900, height = 300)
		
		self.tree = ttk.Treeview(self.container)

		self.tree["columns"]=("one","two")
		self.tree.column("one", width=100 )
		self.tree.column("two", width=800)
		self.tree.heading("one", text="Username")
		self.tree.heading("two", text="Comment")

		self.menubar = tk.Menu(self.container)
		self.filemenu = tk.Menu(self.menubar, tearoff=0)
		self.filemenu.add_command(label="Quit!", command=self.container.quit)
		self.menubar.add_cascade(label="File", menu=self.filemenu)

		self.processingmenu = tk.Menu(self.menubar, tearoff=0)
		self.processingmenu.add_command(label="Load Comments", command=self.showComments)
		self.menubar.add_cascade(label="Processing", menu=self.processingmenu)
		self.config(menu=self.menubar)
		self.container.pack()
		self.tree.pack()
		self.t1 = threading.Thread(target=self.showComments)
		#funcition to do PRAW calculations
	def showComments(self):
		reddit = praw.Reddit(client_id='XnZ69BVaeiyCOA',
                     client_secret='P1s87TxT4gi-4z7tdLoUbMWh-HI',
                     password='papflap66',
                     user_agent='testscript by /u/HC_interaction_Henk',
                     username='HC_interaction_Henk')
		redditurl = simpledialog.askstring("Input","Please insert Reddit url")
		try:
			submission = reddit.submission(url=redditurl)
			top_level_list = []
			submission.comments.replace_more(limit=None)
			for top_level_comment in submission.comments:
					top_level_list.append(top_level_comment.id)
					self.tree.insert("", 3, top_level_comment.id, text=top_level_comment.id, values=(top_level_comment.author,top_level_comment.body))

			submission.comments.replace_more(limit=None)
			for comment in submission.comments.list():
				if comment.id not in top_level_list:
					self.tree.insert(comment.parent_id[3:], 3, comment.id, text=comment.id, values=(comment.author,comment.body))
		except praw.exceptions.ClientException:
			messagebox.showerror("Error", "This URL is invalid")
# set commentTreeDisplay as root and run as mainloop
root = CommentTreeDisplay()
root.mainloop()
