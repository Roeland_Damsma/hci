#Roeland Damsma & Merlijn van der Baan
#s2972654 & s2757656
import tkinter as tk
from tkinter import ttk
import praw
import threading
import part3
import part2
import queue

class UpdatedTreeDisplay(part3.ResponseCommentTreeDisplay):
	
	def __init__(self, master):
		part3.ResponseCommentTreeDisplay.__init__(self, master)
		self.master = master
		self.reddit = praw.Reddit(client_id='XnZ69BVaeiyCOA',
								  client_secret='P1s87TxT4gi-4z7tdLoUbMWh-HI',
								  password='papflap66',
								  user_agent='testscript by /u/HC_interaction_Henk',
								  username='HC_interaction_Henk')
		self.update_slider =tk.Scale(self.frame, from_=0, to_=100, orient=tk.HORIZONTAL, command=self.setSlider2)
		self.update_slider.pack()
		self.x = tk.IntVar()

	def setSlider2(self, slideVal):
		#slider for speed of updating comments
		self.x = int(slideVal)

	def update_comments(self):
		#function to update comments in treeview
		t = 10001 - (self.update_slider.get() * 100)
		if self.update_slider.get() == 0:
			pass
		else:
			self.redditurl = part2.ProcessComments.get_redditurl(self)
			part2.ProcessComments.clear_queue(self)
			self.frame.tree.delete(*self.frame.tree.get_children())
			part2.CommentTreeDisplay.get_comments(self, self.redditurl)

		self.frame.after(t, self.update_comments)


class UpdateComments:

	def __init__(self, master):
		#sets up threads
		self.master = master
		self.frame = UpdatedTreeDisplay(self.master)
		self.comment_thread = threading.Thread(target=self.frame.update_comments)
		self.comment_thread.start()


root = tk.Tk()
incoming_stream = UpdateComments(root)
root.mainloop()