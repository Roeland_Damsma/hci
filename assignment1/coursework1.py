#!/usr/bin/python3
#Roeland Damsma en Merlijn van der Baan
import tkinter as tk
from tkinter import ttk
import praw 
import time
import threading


#main class incomingsubmissions
class IncomingSubmissions(tk.Tk):

    #initialization of gui window
    def __init__(self):
        tk.Tk.__init__(self)
        self.container = tk.Frame(self,width=900, height=300)
        self.tree=ttk.Treeview(self.container)
        self.tree['show']='headings'
        self.tree['columns']=('Subreddit','Submission')
        self.tree.column('Subreddit', width= 150)
        self.tree.column('Submission', width= 750)
        self.tree.heading('Subreddit', text='Subreddit')
        self.tree.heading('Submission', text='Submission')
        S = ttk.Scrollbar(self.container, orient="vertical", command=self.tree.yview)
        S.pack(side='right', fill='y')
        self.tree.configure(yscrollcommand=S.set)
        self.container.pack()
        self.tree.pack()
        self.x = tk.IntVar()
        #t1 = threading.Thread(target=self.getSubmission)
        #t1.start()
        #t2 = threading.Thread(target=self.setSlider)
        #t2.start()

root = IncomingSubmissions()
root.title('part1.py')
root.mainloop()   
