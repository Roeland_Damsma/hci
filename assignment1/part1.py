#!/usr/bin/python3
#Roeland Damsma & Merlijn van der Baan
#s2972654 & s2757656
import tkinter as tk
from tkinter import ttk
import praw 
import time
import threading
import queue
import re


#main class incomingsubmissions
class IncomingSubmissions(tk.Frame):
 #initialization of gui window
    def __init__(self, master, queue, subreddit):
        #Frame
        tk.Frame.__init__(self,master)
        self.topframe = tk.Frame(self)
        self.myqueue = queue
        self.subreddit = subreddit
        #Toplabel
        self.main_label = tk.Label(text='Welcome')
        self.main_label.pack(side=tk.TOP)
        #slider
        self.slider = tk.Scale(self.topframe, from_=0, to_=100, orient=tk.HORIZONTAL, command=self.setSlider)
        self.slider.set(1)
        self.slider.pack(side=tk.LEFT) 
        #pause button
        self.pause_button = tk.Button(self.topframe, text='Pause', command=self.pause)
        self.pause_button.pack(side=tk.LEFT)
        #treeview
        self.tree=ttk.Treeview(self.master, columns=("namesubreddit", "titlesubmission"))
        self.tree['show']='headings'
        self.tree['columns']=('Subreddit','Submission')
        self.tree.column('Subreddit', width= 150)
        self.tree.column('Submission', width= 750)
        self.tree.heading('Subreddit', text='Subreddit')
        self.tree.heading('Submission', text='Submission')
        #scrollbar
        S = ttk.Scrollbar(self.master, orient="vertical", command=self.tree.yview)
        S.pack(side='right', fill='y')
        self.tree.configure(yscrollcommand=S.set)
        #entry and buttons for white and blacklisting
        self.e1 = tk.Entry(self.topframe)
        self.e1.insert(0, 'enter subreddit')
        self.e1.pack(side=tk.LEFT)
        self.b1 = tk.Button(self.topframe, text='Add to whitelist', command=self.addwhitelist)
        self.b1.pack( side = tk.RIGHT )
        self.b2 = tk.Button(self.topframe, text='Add to blacklist', command = self.addblacklist)
        self.b2.pack(side=tk.RIGHT)
        self.b3 = tk.Button(self.topframe, text='Clear all lists', command=self.clearlists)
        self.b3.pack(side=tk.RIGHT)
        self.whitelist = []
        self.blacklist = []

        self.tree.pack(side=tk.BOTTOM)
        self.topframe.pack()
        self.x = tk.IntVar()
    #sets slider to 0 to pause incoming submissions

    def pause(self):
        self.slider.set(0)

    #whitelist
    def addwhitelist(self):
        self.whitelist.append(self.e1.get())
        self.e1.delete(0, tk.END)
        print(self.whitelist)
    #blacklist

    def addblacklist(self):
        self.blacklist.append(self.e1.get())
    #clear list

    def clearlists(self):
        self.blacklist = []
        self.whitelist = []
    #check if subreddits in black and whitelist actually exist
    
    def subredditExists(self,sub):
        exists = True
        try:
            reddit.subreddits.search_by_name(sub, exact=True)
        except NotFound:
            exists = False
        return exists

    def standardizeText(self,s):
        #remove all non-unicode from a string
        return re.sub(r'[^\u0000-\uFFFF]','■', s)    


    #checks queueu for submissions
    def updateSubmission(self):
        t = 1001 - (self.slider.get()*10)
        if self.slider.get() == 0:
           pass
        else:
            try:
                #check if slider is on 0
                submission = self.myqueue.get(block=False)
                if submission is not None and submission.subreddit not in self.blacklist and self.whitelist == []:
                    self.tree.insert('', 0, submission.id, values=(submission.subreddit.display_name, self.standardizeText(submission.title)))
                elif submission is not None and submission.subreddit in self.whitelist and self.blacklist == []:
                    self.tree.insert('', 0, submission.id, values=(submission.subreddit.display_name, self.standardizeText(submission.title)))
            except queue.Empty: pass
        self.after(t, self.updateSubmission)
    
    #function for the slider
    def setSlider(self, slideVal):
        self.x = int(slideVal)         

     
class Processing:
    #class for all processing and threading
    def __init__(self, master):

        self.master = master
        self.myqueue = queue.Queue()
        self.reddit = praw.Reddit(client_id='XnZ69BVaeiyCOA',
                     client_secret='P1s87TxT4gi-4z7tdLoUbMWh-HI',
                     password='papflap66',
                     user_agent='testscript by /u/HC_interaction_Henk',
                     username='HC_interaction_Henk')
        #get all subreddits 
        self.subreddit = self.reddit.subreddit('all')

        self.frame = IncomingSubmissions(self.master, self.myqueue, self.subreddit)
        self.frame.pack()

        self.thread1 = threading.Thread(target=self.getSubmission)
        self.thread1.start()

        self.thread2 = threading.Thread(target=self.frame.updateSubmission)
        self.thread2.start()
        self.frame.updateSubmission()
        self.master.mainloop()
    #function for putting submissions in queue    
    def getSubmission(self):
        for submission in self.subreddit.stream.submissions():
            self.myqueue.put(submission)

#set incomingsubmissions as root and run root as mainloop


root = tk.Tk()
root.title('part1.py')
incoming_stream = Processing(root)
root.mainloop()





