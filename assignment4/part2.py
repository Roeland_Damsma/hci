#Roeland Damsma - S2972654
#Merlijn van der Baan - S2757656

import tkinter as tk
from tkinter import ttk
import tweepy
import json
import threading
import queue
import re
from tkinter import filedialog
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from tkinter import messagebox


class IncomingTweets(tk.Frame):
    # initialization of GUI window
    def __init__(self, master, queue, api):
        tk.Frame.__init__(self, master)
        self.topframe = tk.Frame(self)
        self.bottomframe = tk.Frame(self)
       
        
        self.myqueue = queue
        self.master = master

        self.tree = ttk.Treeview(self.master)
        self.tree['columns'] = ('User', 'Tweet')
        self.tree.column("User", width=100)
        self.tree.column("Tweet", width=1000)
        self.tree.heading('User', text='User')
        self.tree.heading('Tweet', text='Tweet')
        #amount of participants
        self.p1 = tk.IntVar()
        self.p2 = tk.IntVar()
        self.p2.set(10)
        self.part_label = tk.Label(self.bottomframe, text = 'Participants').grid(column=1, row=0)
        self.min_part_label = tk.Label(self.bottomframe, text='minimum').grid(column=0,row=1)
        self.min_part = tk.Entry(self.bottomframe, textvariable=self.p1).grid(column=1, row=1)
        self.max_part_label = tk.Label(self.bottomframe, text='maximum').grid(column=0, row=2)
        self.max_part = tk.Entry(self.bottomframe, textvariable=self.p2).grid(column=1, row=2)
       
        #conversation length

        self.c1 = tk.IntVar()
        self.c2 = tk.IntVar()
        self.c2.set(10)
        self.conv_label = tk.Label(self.bottomframe, text='conversations length').grid(column=1, row=3)
        self.min_conv_label = tk.Label(self.bottomframe, text='minimum').grid(column=0, row=4)
        self.min_conv = tk.Entry(self.bottomframe, textvariable=self.c1).grid(column=1, row=4)
        self.max_conv_label = tk.Label(self.bottomframe, text='maximum').grid(column=0, row=5)
        self.max_conv = tk.Entry(self.bottomframe, textvariable=self.c2).grid(column=1, row=5)

        self.neg_slider_label = tk.Label(self.bottomframe, text='Negativity').grid(column=5, row=0)
        self.neg_slider = tk.Scale(self.bottomframe, from_=0, to_=100, orient=tk.VERTICAL)
        self.neg_slider.set(0)
        self.neg_slider.grid(column = 5, row=1, rowspan=3)

        self.pos_slider_label = tk.Label(self.bottomframe, text='Positivity').grid(column=6, row=0)
        self.pos_slider = tk.Scale(self.bottomframe, from_=0, to_=100, orient=tk.VERTICAL)
        self.pos_slider.set(0)
        self.pos_slider.grid(column=6, row=1, rowspan=3)


        S = ttk.Scrollbar(self.master, orient="vertical", command=self.tree.yview)
        S.pack(side='right', fill='y')

        self.b = tk.Button(self.bottomframe, text="Update", command=self.loadTweets).grid(column=1, row=6)
        
        self.tree.configure(yscrollcommand=S.set)
        self.menubar = tk.Menu(self.topframe)
        self.filemenu = tk.Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label="Quit!", command=self.master.quit)
        self.filemenu.add_command(label="Open", command=self.loadFile)
        self.menubar.add_cascade(label="File", menu=self.filemenu)
        self.menubar.add_command(label="Help",command=self.help)
        self.bottomframe.pack()

        self.master.config(menu=self.menubar)
        self.tree.pack()

    def help(self):
        messagebox.showinfo("Help", "Open a file from the file menu. To specify the type of conversation, use the control panel at the bottom of the screen")

    def standardizeText(self, s):
        # remove all non-unicode from a string
        std1 = re.sub(r'[^\x00-\x7F]+', ' ', s[0])
        std2 = re.sub(r'[^\x00-\x7F]+', ' ', s[1])
        newtup = (std1, std2, s[2], s[3])
        return newtup

    def loadFile(self):
        self.myjsonfilename = tk.filedialog.askopenfilename(title ="Select File", filetypes=(("json files", "*.json"),))
        if len(self.myjsonfilename) > 0:
            self.loadTweets()

        else:
            pass

    def loadTweets(self):
        self.myfile = open(self.myjsonfilename, 'r')
        self.tree.delete(*self.tree.get_children())
        score_list = []
        sid = SentimentIntensityAnalyzer()
        for convo in self.myfile.readlines():
            unique = []
            self.scores = []
            lines = json.loads(convo)
            for line in lines:
                if line[0] not in unique:
                    unique.append(line[0])
            if len(lines) >= self.c1.get() and len(lines) <= self.c2.get() and len(unique) >= self.p1.get() and len(
                    unique) <= self.p2.get():
                for line in lines:
                    ss = sid.polarity_scores(line[1])
                    if ss['neg']*100 >= self.neg_slider.get() and ss['pos']*100 >= self.pos_slider.get():
                        self.scores.append(1)
                    else:
                        self.scores.append(0)
                if 0 not in self.scores:
                    self.insertTweet(lines)

    def insertTweet(self,lines):
        parent = lines.pop(0)
        parent = self.standardizeText(parent)
        self.tree.insert("", "end", str(parent[3]), text=str(parent[3]), values=(parent[0], parent[1]))
        for tweet in lines:
            tweet = self.standardizeText(tweet)
            self.tree.insert(str(tweet[2]), "end", str(tweet[3]), text=str(tweet[3]),
                             values=(tweet[0], tweet[1]))

    def set_credentials(self, filename):
        self.cred = open(filename, 'r')
        self.cred = self.cred.readlines()
        self.conskey = self.cred[0].rstrip()
        self.conssec = self.cred[1].rstrip()
        self.acckey = self.cred[2].rstrip()
        self.accsec = self.cred[3].rstrip()

    def standardizeText(self, s):
        # remove all non-unicode from a string
        std1 = re.sub(r'[^\x00-\x7F]+', ' ', s[0])
        std2 = re.sub(r'[^\x00-\x7F]+', ' ', s[1])
        newtup = (std1, std2, s[2], s[3])
        return newtup

    def updateTweets(self):
        try:

            tweetlist = self.myqueue.get(block=False)


            if tweetlist is not None:
                parent = tweetlist.pop(0)
                parent = self.standardizeText(parent)
                self.tree.insert("", "end", str(parent[3]), text=str(parent[3]), values=(parent[0], parent[1]))
                for tweet in tweetlist:
                    tweet = self.standardizeText(tweet)
                    self.tree.insert(str(tweet[2]), "end", str(tweet[3]), text=str(tweet[3]), values=(tweet[0], tweet[1]))
        except queue.Empty:
            pass
        self.after(1000, self.updateTweets)


class MyStreamListener(tweepy.StreamListener):

    def __init__(self, queue):
        self.myqueue = queue
        self.consumer_key = "svhOZ1pi8IkdFdbTPkKJl1lb1"
        self.consumer_secret = "zNzSb74CgzHBd23iz2538IQkMFJiafvNVfcqFbQS8zZA2k6Ocf"
        self.access_token_key = "1105442560972075008-Wl7QhCR0ihmxTmDbvOlM0M1J8JalTJ"
        self.access_token_secret = "yeupJraBuoxQrI05nAL87BsZpqUljLp5wNZjpUNIu9i0c"
        self.auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
        self.auth.set_access_token(self.access_token_key, self.access_token_secret)

        self.api = tweepy.API(self.auth)

    def on_status(self, status):
        tweetlist = []
        if status.in_reply_to_status_id is not None:
            tweetlist.append((status.user.name, status.text, status.in_reply_to_status_id, status.id))
            try:
                while status.in_reply_to_status_id is not None:
                    status = self.api.get_status(status.in_reply_to_status_id)
                    tweetlist.insert(0, (status.user.name, status.text, status.in_reply_to_status_id, status.id))
            except tweepy.TweepError:
                pass

            if len(tweetlist) <= 10 and len(tweetlist) >= 3:
                self.myqueue.put(tweetlist)


class Processing:
    # class for all processing and threading
    def __init__(self, master):
        self.master = master
        self.myqueue = queue.Queue()
        self.consumer_key = "svhOZ1pi8IkdFdbTPkKJl1lb1"
        self.consumer_secret = "zNzSb74CgzHBd23iz2538IQkMFJiafvNVfcqFbQS8zZA2k6Ocf"
        self.access_token_key = "1105442560972075008-Wl7QhCR0ihmxTmDbvOlM0M1J8JalTJ"
        self.access_token_secret = "yeupJraBuoxQrI05nAL87BsZpqUljLp5wNZjpUNIu9i0c"
        self.auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
        self.auth.set_access_token(self.access_token_key, self.access_token_secret)

        self.api = tweepy.API(self.auth)
        self.myStreamListener = MyStreamListener(self.myqueue)
        self.myStream = tweepy.Stream(auth=self.api.auth, listener=self.myStreamListener)

        self.frame = IncomingTweets(self.master, self.myqueue, self.api)
        self.frame.pack()

        #self.thread1 = threading.Thread(target=self.getTweet)
        #self.thread1.start()

        self.thread2 = threading.Thread(target=self.frame.updateTweets)
        self.thread2.start()
        self.frame.updateTweets()
        self.master.mainloop()

    def getTweet(self):
        self.tweet_words = ["the"]
        self.tweet_lang = ["en"]
        self.myStreamListener = MyStreamListener(self.myqueue)
        self.myStream = tweepy.Stream(auth=self.api.auth, listener=self.myStreamListener)
        self.myStream.filter(track=self.tweet_words, languages=self.tweet_lang)


root = tk.Tk()
root.title("part1.py")
twitter = Processing(root)
root.mainloop()
