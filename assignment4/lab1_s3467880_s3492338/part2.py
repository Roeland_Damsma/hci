#!/usr/bin/python3
import praw, tkinter as tk
from tkinter import ttk
from tkinter import simpledialog
import threading;
import sys;
from praw.models import MoreComments

def clr(string):
    return_string = ""
    for char in string:
        if ord(char) <= 65535:
            return_string += char
    return return_string

class CommentTreeDisplay(tk.Frame):
    def __init__(self, parent, root, *args, **kwargs):
        tk.Frame.__init__(self, parent)
        
        # Create a menubar
        self.menubar = MenuBar(self)
        root.config(menu=self.menubar)
        
        # Reddit info
        self.reddit = praw.Reddit(client_id = 'n6GFlMqHMlpZXA',
        client_secret = 'vFRumNy6Qc2f7Gz7n4xCSLPaN0c',
        user_agent = 'linuxmint:HCI2019:v1.0.0 (by /u/hcireddit)')
        
        # Tree
        self.tree_obj = CommentTree(self)
        self.tree_obj.pack(side='top', fill="both",expand=True)
        
        
class CommentTree(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, parent)
        #self.topframe = tk.Frame(self)
        self.parent = parent

        self.currentcomments=set([])
        
        self.tree = ttk.Treeview(self)
        self.tree.tag_configure('ttk')
        style = ttk.Style(parent)
        style.configure('Treeview', rowheight=40)

        self.vsb = ttk.Scrollbar(self, orient="vertical", command=self.tree.yview)
        self.tree.configure(yscroll = self.vsb.set)
        self.vsb.pack(side="right", fill="y")
        self.tree.pack(side="left",fill="both",expand=True)
        
        #self.topframe.pack(fill="both",expand=True)
        
    
    def start_task(self,url):
        threading.Thread(target=self.showComments, args=[url]).start()
        
    def showComments(self,url):
        #clear 
        self.tree.delete(*self.tree.get_children())
        #get submission
        reddit = self.parent.reddit
        submission = reddit.submission(url=url)
        submission.comments.replace_more(limit=None)
        #recreate tree
        for comment in submission.comments:
            try:
                node_id = self.tree.insert("", "end", comment.id, text=clr(comment.body))
                self.AddCommentReplies(node_id, comment.replies) 
            except tk.TclError:
                pass

        for comment in submission.comments.list():
            self.currentcomments.add(comment.id)
    def AddCommentReplies(self,node,replies):
        list_replies = list(replies)
        if len(list_replies) > 0:
            for comment in list_replies:
                node_id = self.tree.insert(node, "end", comment.id, text=clr(comment.body))
                self.AddCommentReplies(node_id, comment.replies)
    
class MenuBar(tk.Menu):
    def __init__(self, parent):
        tk.Menu.__init__(self, parent)
        self.parent = parent
        self.url = "test"
        fileMenu = tk.Menu(self, tearoff=False)
        processingMenu = tk.Menu(self, tearoff=False)
        self.add_cascade(label="File",underline=0, menu=fileMenu)
        fileMenu.add_command(label="Exit", underline=1, command=self.quit)
        self.add_cascade(label="Processing",underline=0, menu=processingMenu)
        processingMenu.add_command(label="Load comments", underline=1, command=self.request_url)
        

    def quit(self):
        sys.exit(0)
        
    def request_url(self):
        root = self.parent
        url = simpledialog.askstring("Input", "Which url to look for?", parent=root)
        if url is not None:
            self.url = url
            self.parent.tree_obj.start_task(url)
            

class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        #CommentTree
        self.comment_tree_display = CommentTreeDisplay(self,self)
        self.comment_tree_display.pack(fill="both",expand=True)
            
def main():
    app = App()
    app.mainloop()
    
if __name__ == "__main__":
    main()
