#!/usr/bin/python3

import praw, tkinter as tk
from tkinter import ttk
from prawcore import NotFound
from tkinter import messagebox
import threading;

class IncomingSubmissions(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, parent)

        # Filter
        self.filter_obj = Filter(self)
        self.filter_obj.pack(side="bottom",fill="both")

        # Tree
        self.tree_obj = Tree(self, self.filter_obj)
        self.tree_obj.pack(side="top",fill="both",expand=True)
        
        self.after(0,self.tree_obj.start_submission_thread)

        # Reddit info
        self.reddit = praw.Reddit(client_id = 'n6GFlMqHMlpZXA',
        client_secret = 'vFRumNy6Qc2f7Gz7n4xCSLPaN0c',
        user_agent = 'linuxmint:HCI2019:v1.0.0 (by /u/hcireddit)')
        
class Filter(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, parent)
        self.parent=parent
        self.filtermode = tk.IntVar()
        self.topframe = tk.Frame(self)
        
        # List
        self.tree_list = ttk.Treeview(self.topframe)
        self.vsb = tk.Scrollbar(self.topframe, orient="vertical",command=self.tree_list.yview)
        self.tree_list.configure(yscrollcommand=self.vsb.set)
        self.vsb.pack(side="right", fill="y")
        self.tree_list.pack(fill="x", expand=False)
        self.topframe.pack(fill="x", expand=False)
        
        self.tree_list.bind("<Double-1>", self.start_task_remove_list)
        
        # Entry
        self.bottomframe = tk.Frame(self)
        self.entry = tk.Entry(self.bottomframe)
        self.entry.pack(side="left",expand=True,fill='both')
        self.button = tk.Button(self.bottomframe,text="Add",command=self.add_white_list)
        self.button.pack(side="right",fill='both')
        self.setblacklist = tk.Radiobutton(self.topframe,text="blacklist these subreddits",variable=self.filtermode,value=0)
        self.setblacklist.pack(side="top",fill="y")
        self.setwhitelist = tk.Radiobutton(self.topframe,text="whitelist these subreddits",variable=self.filtermode,value=1)
        self.setwhitelist.pack(side="top",fill="y")
        self.bottomframe.pack(fill='x')
        self.filterlist= []

    def get_filterinfo(self):
        return (self.filterlist, self.filtermode.get()) 
        
    def start_task_add_list(self):
        threading.Thread(target=self.add_white_list).start()
        
    def start_task_remove_list(self, event):
        if len(self.tree_list.get_children()) > 0:
            item = self.tree_list.selection()[0]
            threading.Thread(target=self.remove_white_list, args=(item,)).start()

    def sub_exists(self,sub):
        exists = True
        try:
            self.parent.reddit.subreddits.search_by_name(sub, exact=True)
        except NotFound:
            exists = False

        return exists
        
    def add_white_list(self):
        if self.sub_exists(self.entry.get()):
            self.tree_list.insert('', 0, text=self.entry.get() + "\n")
            self.filterlist.append(self.entry.get().lower())
            self.entry.delete(0, tk.END)
        else:
            tk.messagebox.showinfo("Error","subreddit does not exist")
            
    def remove_white_list(self,item):
        self.tree_list.delete(item)
        
class Tree(tk.Frame):
    def __init__(self, parent, filter_obj, *args, **kwargs):
        tk.Frame.__init__(self, parent)
        self.parent = parent
        
        self.filter_obj = filter_obj
        self.tree = ttk.Treeview(self)
        self.tree['show'] = 'headings'
        self.tree["columns"]=("one","two")
        self.tree.column("one", width = 50)
        self.tree.heading("one", text="Subreddit")
        self.tree.heading("two", text="Submission")
        self.tree.tag_configure('ttk')

        self.vsb = ttk.Scrollbar(self.tree, orient="vertical", command=self.tree.yview)
        self.tree.configure(yscroll = self.vsb.set)
        self.vsb.pack(side="right", fill="y")

        #speed scale
        self.scalevar = tk.IntVar()
        self.scalevar.set(5000)
        self.s1 = tk.Scale(self, orient="vertical", from_=1000, to=10000, length = 500, variable=self.scalevar,command=self.updatelabel)
        self.s1.lbl = ttk.Label(parent)
        self.s1.lbl.pack(side="bottom")
        self.s1.pack(side="left")
        self.updatelabel(0)
        
        self.tree.pack(side="left",fill="both",expand=True)

        #pause button
        self.paused = False
        self.pausebuttontext = tk.StringVar()
        self.pausebuttontext.set("pause stream")
        self.pausebutton = tk.Button(self,textvariable=self.pausebuttontext,command=self.pausestream)
        self.pausebutton.pack(side="right",expand=False)
        
    def pausestream(self):       
        if not self.paused:
            self.paused = True
            self.pausebuttontext.set("play stream   ")
        else:
            self.paused = False
            self.pausebuttontext.set("pause stream")
    #updates label for every tick of the scale
    def updatelabel(self, value):
        self.s1.lbl.configure(text="checking for new reddit posts every {0}ms".format(self.scalevar.get()))
    
    def start_submission_thread(self):
        self.thread1 = threading.Thread(target=self.get_submission)
        self.thread1.start()
        
    def get_submission(self):
        if not self.paused:
            filterlist=(self.filter_obj.get_filterinfo())[0]
            filtermode=(self.filter_obj.get_filterinfo())[1]
            for submission in self.parent.reddit.subreddit('all').new(limit=1):
                if not filtermode and submission.subreddit.display_name.lower() in filterlist:
                    continue
                elif filtermode and submission.subreddit.display_name.lower() not in filterlist:
                    continue
                else:
                    try:
                        subreddit_name = "r/" + submission.subreddit.display_name
                        submission_title = submission.title
                        submission_id = submission.id
                        if not self.tree.exists(submission_id):
                            self.tree.insert('', 0, submission_id, values=(subreddit_name,submission_title))
                    except tk.TclError:
                        pass
            
        self.after(self.scalevar.get(), self.get_submission)
 
        
def main():
    root = tk.Tk()
    root.geometry("640x640")
    incsub = IncomingSubmissions(root)
    incsub.pack(side="left",fill="both",expand=True)
    root.mainloop()
    
if __name__ == "__main__":
    main()
