#!/usr/bin/python3
import part3
import praw, tkinter as tk
from tkinter import ttk
from tkinter import simpledialog
from tkinter import messagebox
import time
import threading;

class UpdatedTreeDisplay(tk.Frame):
    def __init__(self, parent, root, *args, **kwargs):
        tk.Frame.__init__(self, parent)
        self.parent = parent
        self.topwindow = tk.Toplevel(self)
        self.topwindow.wm_title("slider")
        # Reddit info
        self.reddit = praw.Reddit(client_id = 'n6GFlMqHMlpZXA',
        client_secret = 'vFRumNy6Qc2f7Gz7n4xCSLPaN0c',
        user_agent = 'linuxmint:HCI2019:v1.0.0 (by /u/hcireddit)',
        username='hcireddit',
        password='topkek')
        
       #CommentTree
        self.comment_tree_display = part3.ResponseCommentTreeDisplay(parent, root)
        self.comment_tree_display.pack(fill="both",expand=True)
        self.tree = self.comment_tree_display.comment_tree_display.tree_obj.tree
        self.parent.after(0, self.startChecking)

        #speed scale
        self.scalevar = tk.IntVar()
        self.scalevar.set(5000)
        self.updatescale = tk.Scale(self.topwindow, orient="vertical", from_=1000, to=10000,length = 500, variable=self.scalevar)
        self.updatescale.pack(fill="both",side="left")

    def startChecking(self):
        threading.Thread(target=self.check).start()

    def check(self):
        try: 
            submission_url = self.comment_tree_display.comment_tree_display.menubar.url
            submission = self.reddit.submission(url=submission_url)
            submission.comments.replace_more(limit=None)
            commentset = set([])
            for comment in submission.comments.list():
                commentset.add(comment.id)              
            if len(commentset) != len(self.comment_tree_display.comment_tree_display.tree_obj.currentcomments):
                self.comment_tree_display.comment_tree_display.tree_obj.showComments(submission_url)
            print(commentset)
        except praw.exceptions.ClientException:
            pass
        
        self.after(self.scalevar.get(), self.check)
        

class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.geometry("640x320") 
        
        # Respond
        self.updated = UpdatedTreeDisplay(self, self)
        self.updated.pack(fill="both",expand=True)
        

def main():
    app = App()
    app.mainloop()
main()
