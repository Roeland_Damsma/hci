#!/usr/bin/python3

import part1
import part3
import tkinter as tk


class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)

        self.notebook = tk.ttk.Notebook(self)
        self.notebook.pack(side='right', fill="both",expand=True)
        
        retree = part3.ResponseCommentTreeDisplay(self,self)
        self.notebook.add(retree, text="main")
        
        self.incsub = part1.IncomingSubmissions(self)
        self.incsub.pack(side='left', fill="both",expand=True)
        
        self.bind("<Double-1>", self.subclicked)
        self.counter = 0

    def subclicked(self,event):
        try:
            sub_id = self.incsub.tree_obj.tree.selection()[0]
            url = str("https://www.reddit.com{0}".format(self.incsub.reddit.submission(sub_id).permalink))
            retree = part3.ResponseCommentTreeDisplay(self,self)
            retree.comment_tree_display.tree_obj.showComments(url)
            self.notebook.add(retree, text="tab " + str(self.counter))
            self.counter += 1
        except IndexError:
            pass

def main():
    app = App()
    app.geometry("1280x640")
    app.mainloop()
    
if __name__ == "__main__":
    main()
