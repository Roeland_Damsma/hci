#!/usr/bin/python3

import part2
import praw, tkinter as tk
from tkinter import ttk
from tkinter import simpledialog
from tkinter import messagebox
import threading;

class ResponseCommentTreeDisplay(tk.Frame):
    def __init__(self, parent, root, *args, **kwargs):
        tk.Frame.__init__(self, parent)
        self.parent = parent
        
        # Reddit info
        self.reddit = praw.Reddit(client_id = 'n6GFlMqHMlpZXA',
        client_secret = 'vFRumNy6Qc2f7Gz7n4xCSLPaN0c',
        user_agent = 'linuxmint:HCI2019:v1.0.0 (by /u/hcireddit)',
        username='hcireddit',
        password='topkek')
        
       #CommentTree
        self.comment_tree_display = part2.CommentTreeDisplay(self,root)
        self.comment_tree_display.pack(fill="both",expand=True)
        self.tree = self.comment_tree_display.tree_obj.tree
        
        self.tree.bind("<Double-1>", self.TreeClicked)
        
    def start_task(self, event):
        threading.Thread(target=self.TreeClicked).start()
        
    def TreeClicked(self,event):
        if len(self.tree.get_children()) > 0:
            item = self.tree.selection()[0]
            reply_string = simpledialog.askstring("Input", "What is your reply?", parent=self.parent)
            if reply_string is not None:
                if len(reply_string) > 0:
                    try:
                        self.reddit.comment(item).reply(reply_string)
                    except praw.exceptions.APIException:
                        messagebox.showwarning("Warning","You can't reply that often")
                    except:
                        messagebox.showwarning("Warning","Your reply could not be posted")
                else:
                    messagebox.showwarning("Warning","Your reply can't be empty")

class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.geometry("640x320") 
        
        # Respond
        self.respond = ResponseCommentTreeDisplay(self,self)
        self.respond.pack(fill="both",expand=True)
 
            
def main():
    app = App()
    app.mainloop()
    
if __name__ == "__main__":
    main()
