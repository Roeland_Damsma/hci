#Roeland Damsma - S2972654
#Merlijn van der Baan - S2757656

import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
import tweepy
import threading
import queue
import re
import json


class IncomingTweets(tk.Frame):
    # initialization of GUI window
    def __init__(self, master, queue, api):
        tk.Frame.__init__(self, master)
        self.topframe = tk.Frame(self)
        self.myqueue = queue
        self.master = master
        self.bottomframe = tk.Frame(self)

        self.tree = ttk.Treeview(self.master)
        self.tree['columns'] = ('User', 'Tweet')
        self.tree.column("User", width=100)
        self.tree.column("Tweet", width=1000)
        self.tree.heading('User', text='User')
        self.tree.heading('Tweet', text='Tweet')

        self.scroll = ttk.Scrollbar(self.master, orient="vertical", command=self.tree.yview)
        self.scroll.pack(side='right', fill='y')

        self.lang_entry = ttk.Entry(self.bottomframe)
        self.lang_entry.insert(0,"en")
        self.lang_entry.grid(column=1,row=0)
        self.lang_entry_label= tk.Label(self.bottomframe,text='Language').grid(column=0, row=0)

        self.word_entry = ttk.Entry(self.bottomframe)
        self.word_entry.insert(0,"the")
        self.word_entry.grid(column=1, row=1)
        self.word_entry_label= tk.Label(self.bottomframe, text='Enter word').grid(column=0,row=1)
        self.b = ttk.Button(self.bottomframe, text="Start", command=self.startTwitter).grid(column= 2, row=0)

        self.menubar = tk.Menu(self.topframe)
        self.filemenu = tk.Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label="Quit!", command=self.master.quit)
        self.menubar.add_cascade(label="File", menu=self.filemenu)

        self.master.config(menu=self.menubar)
        self.topframe.pack()
        self.tree.pack()
        self.bottomframe.pack()
        self.api = api

    def startTwitter(self):
        self.thread1 = threading.Thread(target=self.getTweet)
        self.thread1.start()

    def getTweet(self):
        self.tree.delete(*self.tree.get_children())
        self.tweet_words = self.word_entry.get().split()
        self.tweet_lang = self.lang_entry.get().split()
        self.myStreamListener = MyStreamListener(self.myqueue,self.api)
        self.myStream = tweepy.Stream(auth=self.api.auth, listener=self.myStreamListener)
        self.myStream.filter(track=self.tweet_words, languages=self.tweet_lang)

    def set_credentials(self, filename):
        self.cred = open(filename, 'r')
        self.cred = self.cred.readlines()
        self.conskey = self.cred[0].rstrip()
        self.conssec = self.cred[1].rstrip()
        self.acckey = self.cred[2].rstrip()
        self.accsec = self.cred[3].rstrip()

    def standardizeText(self, s):
        # remove all non-unicode from a string
        std1 = re.sub(r'[^\x00-\x7F]+', ' ', s[0])
        std2 = re.sub(r'[^\x00-\x7F]+', ' ', s[1])
        newtup = (std1, std2, s[2], s[3])
        return newtup

    def updateTweets(self):
        try:

            tweetlist = self.myqueue.get(block=False)

            if tweetlist is not None:
                filename = '_'.join(self.tweet_words) + '-' + '_'.join(self.tweet_lang) + '.json'
                json_string = json.dumps(tweetlist)
                text_file = open(filename, 'a')
                text_file.write(json_string + "\n")
                text_file.close()
                parent = tweetlist.pop(0)
                parent = self.standardizeText(parent)
                self.tree.insert("", "end", str(parent[3]), text=str(parent[3]), values=(parent[0], parent[1]))
                for tweet in tweetlist:
                    tweet = self.standardizeText(tweet)
                    self.tree.insert(str(tweet[2]), "end", str(tweet[3]), text=str(tweet[3]), values=(tweet[0], tweet[1]))
        except queue.Empty:
            pass
        self.after(1000, self.updateTweets)


class MyStreamListener(tweepy.StreamListener):

    def __init__(self, queue, api):
        self.myqueue = queue
        self.api = api

    def on_status(self, status):
        tweetlist = []
        if status.in_reply_to_status_id is not None:
            tweetlist.append((status.user.name, status.text, status.in_reply_to_status_id, status.id))
            try:
                while status.in_reply_to_status_id is not None:
                    status = self.api.get_status(status.in_reply_to_status_id)
                    tweetlist.insert(0, (status.user.name, status.text, status.in_reply_to_status_id, status.id))
            except tweepy.TweepError:
                pass

            if len(tweetlist) <= 10 and len(tweetlist) >= 3:
                self.myqueue.put(tweetlist)


class Processing:
    # class for all processing and threading
    def __init__(self, master):
        self.master = master
        self.myqueue = queue.Queue()
        self.filename = filedialog.askopenfilename(initialdir="/", title="Select Credentials file",
                                                   filetypes=(("txt files", "*.txt"), ("all files", "*.*")))
        self.credfile = open(self.filename, "r")
        self.credfile = self.credfile.readlines()
        self.consumer_key = self.credfile[0].rstrip()
        self.consumer_secret = self.credfile[1].rstrip()
        self.access_token_key = self.credfile[2].rstrip()
        self.access_token_secret = self.credfile[3].rstrip()
        self.auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
        self.auth.set_access_token(self.access_token_key, self.access_token_secret)

        self.api = tweepy.API(self.auth)
        self.myStreamListener = MyStreamListener(self.myqueue, self.api)
        self.myStream = tweepy.Stream(auth=self.api.auth, listener=self.myStreamListener)

        self.frame = IncomingTweets(self.master, self.myqueue, self.api)
        self.frame.pack()



        self.thread2 = threading.Thread(target=self.frame.updateTweets)
        self.thread2.start()
        self.frame.updateTweets()
        self.master.mainloop()


root = tk.Tk()
root.title("part1.py")
twitter = Processing(root)
root.mainloop()
