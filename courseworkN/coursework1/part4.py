#!/usr/bin/python3
# Younes Moustaghfir & Nick van Unen
# Human Computer Interaction Coursework 1 2018
# Information Sience Year 2

import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import praw
import time; import datetime; import time; import threading; import queue; import sys;import urllib.request
import part3

class TreeNotebook(tk.Frame):
    """Frame object that creates tabs for every submission"""

    def __init__(self, root):
        """Intialize TreeNotebook object"""
        self.root = root
        tk.Frame.__init__(self, self.root)
        self.n = ttk.Notebook(self.root)

        #root.title("NIGGAS IN REDDIT")
        menubar = tk.Menu(root)
        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu2 = tk.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Exit", command=self.shutdown)
        filemenu2.add_command(label="Load comments", command=self.create_tab)
        menubar.add_cascade(label="File", menu=filemenu)
        menubar.add_cascade(label="Processing", menu=filemenu2)
        root.config(menu=menubar)

        self.tab_list = []
        self.count = 0
        self.n.pack()

    def create_tab(self):
        """Function that creates a seperate windows to fill in a submission url."""
        self.top = tk.Toplevel()
        self.top.title("Input URL")
        self.top.geometry('300x100')
        tk.Label(self.top, text="Give a URL of a submission: ").grid(row=0)
        self.entry = tk.Entry(self.top)
        self.entry.grid(row=1, column=0)
        self.b1 = tk.Button(self.top, text='Go', command= lambda: self.AddResponseCommentTree(self.entry.get())).grid(row=2, column=0)

    def shutdown(self):
        """Shuts down application"""
        sys.exit(0)

    def AddResponseCommentTree(self, url):
        """Add submission to notebook as tab."""
        try:
            self.top.destroy()
        except:
            pass
        self.tab_list.append(ttk.Frame(self.n))
        comment = part3.ResponseCommentTreeDisplay(self.tab_list[self.count], url)
        name = comment.reddit.submission(url=url).title
        self.n.add(self.tab_list[self.count], text=name)
        if comment.http_error == 1:
            self.n.forget(self.tab_list[self.count])
            messagebox.showinfo("Information", "There was an HTTP request error")
        self.count = self.count + 1


#root = tk.Tk()
#comment_tree = TreeNotebook(root)
#root.mainloop()
