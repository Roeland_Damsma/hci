#!/usr/bin/python3
# Younes Moustaghfir & Nick van Unen
# Human Computer Interaction Coursework 1 2018
# Information Sience Year 2

import tkinter as tk
from tkinter import simpledialog
from tkinter import messagebox
from tkinter import ttk
import praw
import time; import datetime; import time; import threading; import queue; import sys
import part2

class ResponseCommentTreeDisplay(part2.CommentTreeDisplayModel):
    """Display the comment tree of a particular submission of a subreddit."""

    def __init__(self, root, url):
        """Initialze CommentTreeDisplayModel object and add double click functionality """
        part2.CommentTreeDisplayModel.__init__(self, root)
        self.url_queue.put(url)
        self.gui.tree.bind("<Double-3>", self.double_click_comment)


    def double_click_comment(self, event):
        """Double click event and response"""
        self.item = self.gui.tree.selection()[0]
        self.comment_obj = self.reddit.comment(id=self.item)
        self.response = tk.simpledialog.askstring('Input',"Please enter a comment ", parent=root)
        # Get thread ready
        self.thread3 = threading.Thread(target=self.update_comments, args=(self.comment_obj,))
        if self.response is not None or " " or "":
            try:
                # In case of PRAW exceptions
                self.comment_obj.reply(self.response)
            except praw.exceptions.APIException:
                # Show error message in case of exception
                messagebox.showerror("420 Error", "Could not place comment")
            if not self.thread3.isAlive():
                # Check if thread3 is not still alive
                # If not, start thread3
                self.thread3.start()
            else:
                pass
        else:
            messagebox.showerror("Error", "Please enter something...")

    def update_comments(self, comment):
        """Put comment in comment queue in order for part2 to add it to the tree """
        self.subreddit_name = comment.subreddit.display_name
        try:
            # Get submission from submission queue
            self.submission1 = self.submission_queue.get(block=False)
        except queue.Empty:
            pass
        for comm in self.reddit.subreddit(self.subreddit_name).stream.comments():
            if comm.parent() == comment.id:
                subm_obj = (comm, self.submission1)
                self.comment_queue.put(subm_obj)

#root = tk.Tk()
#url = 'https://redd.it/77s8k7'
#comment_tree = ResponseCommentTreeDisplay(root, url)
#root.mainloop()
