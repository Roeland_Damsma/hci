#!/usr/bin/python3
# Younes Moustaghfir & Nick van Unen
# Human Computer Interaction Coursework 1 2018
# Information Sience Year 2

import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import praw
import time; import datetime; import time; import threading; import queue; import sys;import urllib.request

class CommentTreeDisplay(tk.Frame):
    """Display the comment tree of a particular submission of a subreddit."""

    def __init__(self, master, queue, queue1):
        """Initialize CommentTreeDisplay object"""
        self.master = master
        self.comment_queue = queue
        self.url_queue = queue1
        tk.Frame.__init__(self, master)
        self.topframe = tk.Frame(self)

        try:
            self.master.title("Younes & Nick")
            menubar = tk.Menu(master)
            filemenu = tk.Menu(menubar, tearoff=0)
            filemenu2 = tk.Menu(menubar, tearoff=0)
            filemenu.add_command(label="Exit", command=self.shutdown)
            filemenu2.add_command(label="Load comments", command=self.load_comments_window)
            menubar.add_cascade(label="File", menu=filemenu)
            menubar.add_cascade(label="Processing", menu=filemenu2)
            master.config(menu=menubar)
        except AttributeError:
            pass

        # Set up treeview
        self.tree = ttk.Treeview(self.topframe, columns=("author","comments"))
        self.tree['show'] = 'headings'
        self.tree.column("author", width=275)
        self.tree.column("comments", width=1000, stretch=1)
        self.tree.heading("author", text="Author", anchor="w")
        self.tree.heading("comments", text="Comments", anchor="w")
        self.tree['height'] = 25

        # Set up scrollbar, doesn't work yet, how to get this in the treeview without screwing it up...
        self.hscrollbar = ttk.Scrollbar(self.topframe, orient='vertical')
        self.hscrollbar.configure(command=self.tree.yview)
        self.tree.configure(yscrollcommand=self.hscrollbar.set)
        self.hscrollbar.pack(side='right', fill='y')

        # Set it like Docter mills said, put witdh of the tree on 5000 so there can be scrolled.
        self.vscrollbar = ttk.Scrollbar(self.topframe, orient='horizontal')
        self.vscrollbar.configure(command=self.tree.xview)
        self.tree.configure(xscrollcommand=self.vscrollbar.set)
        self.vscrollbar.pack(side='bottom', fill='x')

        self.welcome_label = tk.Label(self.master, text="Welcome to the Reddit commenting system, \nload process to view comments and even comment yourself.", pady=5, font='Helvetica 18 bold')
        self.info_label = tk.Label(self.master, text="Double click to open comment and double right click to comment on a comment or submission.",pady=3)
        self.msg = ""
        self.welcome_label.pack()
        self.info_label.pack()
        self.tab = "    "

        self.tree.pack()
        self.topframe.pack()

    def update_comment(self):
        """Update comment and put comment in tkinter Treeview object"""
        try:
            comment = self.comment_queue.get(block=False)
            print(comment)
            if comment is not None:
                comment1 = comment[0]
                comment2 = comment[1]
                subreddit = comment1.subreddit.display_name
                if comment[0].parent() == comment[1]:
                    # Top level comment
                    try:
                        print(1)
                        print(comment1.author, comment1.body)
                        self.tree.insert('', 'end', comment1.id, values=(comment1.author,comment1.body,))
                    except tk.TclError:
                        pass
                else:
                    # Not a top leven comment
                    # Find appropriate comment to link to
                    try:
                        print(2)
                        amount_of_parents = self.countParents(comment1)
                        x = self.tab * amount_of_parents
                        self.tree.insert(comment1.parent(), 'end', comment1.id, values=(comment1.author,x + comment1.body,))
                    except tk.TclError:
                        pass
        except queue.Empty:
            pass
        #self.master.after(100,self.update_comment)

    def countParents(self, comment):
        """Function to calculate the amount of spaces needed per comment"""
        c = 0
        ancestor = comment
        refresh_counter = 0
        while not ancestor.is_root:
            ancestor = ancestor.parent()
            if refresh_counter % 9 == 0:
                ancestor.refresh()
            refresh_counter += 1
            c += 1
        return c

    def shutdown(self):
        """Shuts down application"""
        sys.exit(0)

    def load_comments_window(self):
        """Gives the user the possibility to fill in a submission url in a pop up window."""
        self.top = tk.Toplevel()
        self.top.title("Input URL")
        self.top.geometry('300x100')
        tk.Label(self.top, text="Give a URL of a submission: ").grid(row=0)
        self.entry = tk.Entry(self.top)
        self.entry.grid(row=1, column=0)
        self.b1 = tk.Button(self.top, text='Go', command= lambda: self.url_queue.put(self.entry.get())).grid(row=2, column=0)
        #self.b1.pack()


class CommentTreeDisplayModel:
    """Model for CommentTreeDisplay object. All information and threads are processed in here."""
    __client_id = 'LNhHz1EJXXi5Dw'
    __client_secret = 'HPk7Gt2Wvoc_kskNpgIx0cBPu-c'
    __username = 'rainbowunicorns420'
    __password = 'rainbowunicorns'

    def __init__(self, master):
        """Initialize model."""
        self.master = master

        # setup queue for messaging
        self.comment_queue = queue.Queue()

        # setup queue for submission information
        self.submission_queue = queue.Queue()

        # setup queue for urls
        self.url_queue = queue.Queue()

        # create reddit object
        self.reddit = praw.Reddit(user_agent='Comment Extraction (by /u/rainbowunicorns420)',
                     client_id=self.__client_id, client_secret=self.__client_secret,
                     username=self.__username, password=self.__password)

        # call GUI part of application
        self.gui = CommentTreeDisplay(self.master, self.comment_queue, self.url_queue)
        self.gui.pack()
        #self.submission = self.reddit.submission(url='https://www.reddit.com/r/funny/comments/3g1jfi/buttons/')

        #create threads
        self.thread1 = threading.Thread(target=self.load_comments)
        self.thread1.start()
        # check if queue contains anything
        self.callQueue()

        self.http_error = 0

    def callQueue(self):
        """Function that repeats the update_comment function."""
        self.gui.update_comment()
        self.master.after(100, self.callQueue)

    def showComments(self, submission):
        """Place comment in comment queue and place some information in labels."""
        submission1 = self.reddit.submission(url=submission)
        submission1.comments.replace_more(limit=1)
        subreddit_label = tk.Label(self.master, text="Subreddit: " + submission1.subreddit.title)
        submission_label = tk.Label(self.master, text="Submission: " + submission1.title)
        subreddit_label.pack()
        submission_label.pack()
        for comment in submission1.comments.list():
            comment_object = (comment, submission1)
            self.comment_queue.put(comment_object)
        self.submission_queue.put(submission1)
        #self.showComments(submission)
        self.master.after(100, None)

    def load_comments(self):
        """Checks if the URL is a valid reddit URL, pass the URL to CommentTreeDisplay and destroy pop up."""
        try:
            self.submission_url = self.url_queue.get(block=False)
            if self.submission_url is not None:
                request = urllib.request.Request(self.submission_url)
                request.get_method = lambda: 'HEAD'
                try:
                    urllib.request.urlopen(request)
                    reddit_validation = 'redd.it'
                    reddit_validation2 = 'reddit.com'
                    if reddit_validation or reddit_validation2 in self.submission_url:
                        try:
                            self.gui.top.destroy()
                        except:
                            pass
                        print("url is:",self.submission_url)
                        self.showComments(self.submission_url)
                    else:
                        #tk.Label(self.top, text="This is not a valid URL, please try again").grid(row=3, column=0)
                        messagebox.showinfo("Information", "This is not a valid URL, please try again")
                except urllib.request.HTTPError:
                    self.http_error = 0
        except queue.Empty:
            pass
        self.master.after(100, self.load_comments)

#root = tk.Tk()
#comment_tree = CommentTreeDisplayModel(root)
#root.mainloop()
