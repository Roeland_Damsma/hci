#!/usr/bin/python3
# Younes Moustaghfir & Nick van Unen
# Human Computer Interaction Coursework 1 2018
# Information Sience Year 2

import tkinter as tk
from tkinter import ttk
import praw
import time; import datetime; import time; import threading
import queue; import re

class IncomingSubmission(tk.Frame):
    """Class for incomming submissions"""

    def __init__(self, master, queue, subreddit):
        self.subreddit = subreddit
        self.submissions_queue = queue
        tk.Frame.__init__(self, master)
        self.topframe = tk.Frame(self)

        # Set up scale
        self.speed_label = tk.Label(self.master, text="Welcome")
        self.scale = tk.Scale(self.topframe, from_=100, to=0, orient=tk.HORIZONTAL, command=self.decide_speed)
        self.scale.set(100)
        self.scale.pack()
        self.speed_label.pack()

        # Set up treeview
        self.tree = ttk.Treeview(self.topframe, columns=("namesubreddit", "titlesubmission"))
        self.tree['show'] = 'headings'
        self.tree.column("namesubreddit", width=200)
        self.tree.column("titlesubmission", width=750)
        self.tree.heading("namesubreddit", text="Name Subreddit")
        self.tree.heading("titlesubmission", text="Title Submission")
        self.tree['height'] = 25
        self.tree.pack()
        self.topframe.pack()

    def standardizeText(self,s):
        #remove all non-unicode from a string
        return re.sub(r'[^\u0000-\uFFFF]','■', s)

    def update_submissions(self):
        t = 3001 - self.scale.get() * 30
        if self.scale.get() == 0:
            pass
        else:
            try:
                msg = self.submissions_queue.get(block=False)
                if msg is not None:
                    self.tree.insert('', 0, msg.id, values=(msg.subreddit.display_name,self.standardizeText(msg.title)))
            except queue.Empty:
                pass
        self.after(t,self.update_submissions)

    def decide_speed(self, scale_value):
        self.v = int(scale_value)
        if self.v == 100:
            msg = "The application is running as fast as possible!"
        elif self.v < 2:
            msg = "The appliction is paused."
        else:
            msg = "The application is running at {0}% of the speed".format(self.v)
        self.update_label(msg)

    def update_label(self, msg):
        self.speed_label.configure(text=str(msg))
        self.speed_label.pack()

class IncomingSubmissionModel:
    """Model class for incoming submission"""

    __client_id = 'LNhHz1EJXXi5Dw'
    __client_secret = 'HPk7Gt2Wvoc_kskNpgIx0cBPu-c'
    __username = 'rainbowunicorns420'
    __password = 'rainbowunicorns'

    def __init__(self, master):
        self.master = master

        # setup queue for messaging
        self.submissions_queue = queue.Queue()

        # create reddit object
        self.reddit = praw.Reddit(user_agent='Comment Extraction (by /u/rainbowunicorns420)',
                     client_id=self.__client_id, client_secret=self.__client_secret,
                     username=self.__username, password=self.__password)

        self.subreddit = self.reddit.subreddit("all")

        # call GUI part of application
        self.gui = IncomingSubmission(self.master, self.submissions_queue, self.subreddit)
        self.gui.pack()


        #create threads
        self.thread1 = threading.Thread(target=self.get_stream_submissions)
        self.thread1.start()

        # check if queue contains anything

        self.thread2 = threading.Thread(target=self.gui.update_submissions)
        self.thread2.start()
        self.gui.update_submissions()
        #self.master.mainloop()

    def get_stream_submissions(self):
        for submission in self.subreddit.stream.submissions():
            # fill submissions_list with submissions
            self.submissions_queue.put(submission)

#root = tk.Tk()
#root.title("Nick & Younes Part 1 HCI")
#incomingsub = IncomingSubmissionModel(root)
#root.mainloop()
