#!/usr/bin/python3
# Younes Moustaghfir & Nick van Unen
# Human Computer Interaction Coursework 1 2018
# Information Sience Year 2

import tkinter as tk
from tkinter import ttk
import praw
import time; import datetime; import time; import threading; import queue; import sys;import urllib.request
import part1; import part4; import part3; import part2;

class Notebook(tk.Frame):

    def __init__(self, root):
        self.root = root

        root.state("normal")
        tk.Frame.__init__(self, root)
        self.submissions = part1.IncomingSubmissionModel(root)
        self.comments = part4.TreeNotebook(root)

        self.submissions.gui.tree.bind("<Double-1>", self.double_click_submission)

        #self.submissions.gui.grid(row=0, column=0, sticky="nsew")
        #self.comments.grid(row=1, column=0, sticky="nsew")

        #root.rowconfigure(0, weight=1)
        #root.rowconfigure(1, weight=1)
        #root.columnconfigure(0, weight=1)
        #root.columnconfigure(1, weight=1)

        self.submissions.gui.pack(side="left")
        self.comments.pack(side="left")

    def double_click_submission(self, event):
        """Double click event and response"""
        self.item = self.submissions.gui.tree.selection()[0]
        submission = self.submissions.reddit.submission(id=self.item)
        submission_url = "https://www.reddit.com" + submission.permalink
        self.comments.AddResponseCommentTree(submission_url)


root = tk.Tk()
root.title("Nick & Younes HCI")
incomingsub = Notebook(root)
root.mainloop()
